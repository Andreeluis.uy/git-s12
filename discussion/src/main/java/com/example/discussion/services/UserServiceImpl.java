package com.example.discussion.services;

import com.example.discussion.models.User;
import com.example.discussion.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;


    //create
    public void createUser(User user){

        userRepository.save(user);
    }

    //read
    public Iterable<User> getUser(){

        return userRepository.findAll();
    }

    //delete
    public ResponseEntity deleteUser(Long id){
        userRepository.deleteById(id);
        return new ResponseEntity<>("User Deleted Successfully", HttpStatus.OK);
    }

    //update
    public ResponseEntity updateUser(Long id,User user){
        User userForUpdating = userRepository.findById(id).get();
        userForUpdating.setUsername(user.getUsername());
        userForUpdating.setPassword(user.getPassword());
        userRepository.save(userForUpdating);
        return  new ResponseEntity<>("User Updated Successfully", HttpStatus.OK);
    }
}